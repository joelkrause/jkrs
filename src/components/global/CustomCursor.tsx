import React, {useState, useEffect } from 'react';
import styled from 'styled-components'


const CustomCursor = () => {
    const [cursorData,setCursorData] = useState({
        scale: 1,
        hovered: false,
        targets: ['a', 'button', 'input', 'label']        
    })

    const setStateValue = (id,value) => {
        setCursorData(prevState => ({
            ...prevState,
            [id]: value,
        }))
    }

    useEffect(() => {
        window.addEventListener('mousemove', (e) => {
            document.documentElement.style.setProperty('--custom-cursor-y-position', `${e.clientY}px`)
            document.documentElement.style.setProperty('--custom-cursor-x-position', `${e.clientX}px`)
            
            var element = e.target as HTMLElement;
            const ele = element.tagName.toString().toLowerCase()

            if ((cursorData.targets.length > 0 && cursorData.targets.includes(ele)) || cursorData.targets.includes(ele)) {
                setStateValue('hovered',true)
            } else {
                setStateValue('hovered',false)
            }
        })
    }, []);

    return (
        <>
        <CursorEle className={cursorData.hovered ? 'hovered' : ''}/>
        </>
    );
};

export default CustomCursor;

const CursorEle = styled.div`
    display: block;
    pointer-events: none;
    position: fixed;
    z-index: 99;
    width: 25px;
    height: 25px;
    border: 1px solid var(--custom-cursor-border-color);
    transform: translate(-100%, -100%);
    transition: all 0.25s cubic-bezier(0.23, 1, 0.32, 1);
    border-radius: 50%;
    mix-blend-mode: var(--custom-cursor-mix-blend-mode);

    // Set data from mousemove event
    transform:translate(var(--custom-cursor-x-position),var(--custom-cursor-y-position));

    // Set hovered state...
    &.hovered {
        width: 45px;
        height: 45px;
        background: var(--custom-cursor-background-color);        
    }
`