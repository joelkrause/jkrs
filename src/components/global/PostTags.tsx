import React from 'react';
import styled from 'styled-components'

const PostTags = ({tags}) => {
    return (
        <PostTagsWrapper>
            {tags.map(node => {
                return (
                    <PostTag key={node.uuid}>
                        {node.name}
                    </PostTag>
                )
            })}            
        </PostTagsWrapper>
    );
};

export default PostTags;

const PostTagsWrapper = styled.div`
    display:flex;
    align-items:center;
    gap:10px;
    margin-left: auto;
`

const PostTag = styled.div`
    color:#fff;
    padding:0.3rem 0.5rem;
    font-size:0.975rem;
    line-height:1;
    border-radius:3px;
    display:flex;
    align-items:center;
    flex-wrap:wrap;
    white-space:nowrap;

    [data-color-scheme="light"] & {
      background-color:#ccc;
    }
    [data-color-scheme="dark"] & {
        background:#333;
    }
`