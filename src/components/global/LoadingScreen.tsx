import React, {useEffect} from 'react';
import { getHours } from 'date-fns'
import gsap from 'gsap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMugHot, faSun, faMoon } from '@fortawesome/free-solid-svg-icons'

const LoadingScreen = () => {
    useEffect(() => {
        const timeline = gsap.timeline();
        timeline.to('.loading_screen-heading h1',{opacity:1,y:0,delay:1})
        timeline.to('.loading_screen-heading h1',{opacity:0,y:-85,delay:2})
        timeline.to('.loading_screen',{autoAlpha:0})
        setTimeout(() => {
            const headerEle = document.querySelector('.site-header')
            document.documentElement.style.setProperty('--header-height', `${headerEle.clientHeight}px`)
        }, 3000);
    },[]);

    const timeOfDay = () => {
        const hour = getHours(new Date())
        if(hour < 12){
            return {
                text:'Good Morning!',
                icon:faMugHot
            }
        } else if(hour >= 12 && hour <= 17){
            return {
                text:'Good Afternoon!',
                icon:faSun
            }
        } 
        return {
            text:'Good Evening!',
            icon: faMoon
        }
    }
    return (
        <div className="loading_screen">
            <div className="loading_screen-heading">
                <h1><FontAwesomeIcon icon={timeOfDay().icon} />{timeOfDay().text}</h1>
            </div>
        </div>
    );
};

export default LoadingScreen;