import React from 'react';
import {Link} from 'gatsby'
import styled from 'styled-components'

import PostTags from './PostTags'


const PostListItem = ({post}) => {
    return (
        <PostItem to={`/${post.full_slug}`}>
            {post.content.post_icon &&
                <PostIcon>
                    <img src={post.content.post_icon} />
                </PostIcon>
            }
            <PostName>{post.name}</PostName>
            <PostTags tags={post.content.categories} />
      </PostItem>
    );
}

export default PostListItem;

const PostItem = styled(props => <Link {...props} />)`
    display:flex;
    align-items:center;
    padding:1.25rem 1rem;
    border-radius:3px;
    margin: 0 -1rem;
    transition: all 0.25s;

    &:hover {
        background:#f8f8f8;
        
        [data-color-scheme="light"] & {
            background:#f8f8f8;
        }
        [data-color-scheme="dark"] & {
            background:#333;
        }
    }

    &:not(:last-of-type){
        margin:0 -1rem 1rem;
    }
`

const PostIcon = styled.div`
    padding-right:1.5rem;
    img {
        width:35px;
        height:auto;
        display:block;
    }
`

const PostName = styled.div`
    font-size:1.25rem;
    margin:0 1rem 0;
    line-height: 1;
`